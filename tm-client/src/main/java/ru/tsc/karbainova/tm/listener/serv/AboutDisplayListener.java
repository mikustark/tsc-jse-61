package ru.tsc.karbainova.tm.listener.serv;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;

@Component
public class AboutDisplayListener extends AbstractSystemListener {
    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "About dev";
    }

    @Override
    @EventListener(condition = "@aboutDisplayListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println(propertyService.getDeveloperName());
        System.out.println(Manifests.read("developer"));
        System.out.println(propertyService.getDeveloperEmail());
    }
}
