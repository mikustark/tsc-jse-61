package ru.tsc.karbainova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.karbainova.tm.dto.LogDto;

public interface IJmsService {

    void send(@NotNull LogDto entity);

    LogDto createMessage(@NotNull Object object, @NotNull String type);

}
