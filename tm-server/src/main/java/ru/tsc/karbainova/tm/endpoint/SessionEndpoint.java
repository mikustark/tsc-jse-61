package ru.tsc.karbainova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
@NoArgsConstructor
public class SessionEndpoint extends AbstractEndpoint {

    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password
    ) {
        return sessionService.open(login, password);
    }

    @WebMethod
    public void closeSession(@WebParam(name = "session") final SessionDTO session) {
        sessionService.validate(session);
        sessionService.close(session);
    }
}
