package ru.tsc.karbainova.tm.service.model;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.repository.model.ProjectRepository;
import ru.tsc.karbainova.tm.api.service.model.IProjectServiceModel;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.User;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectServiceModel {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Transactional
    @SneakyThrows
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Transactional
    @SneakyThrows
    public void clear() {
        projectRepository.clear();
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<Project> collection) {
        if (collection == null) return;
        for (Project i : collection) {
            add(i);
        }
    }

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @SneakyThrows
    public Project add(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.save(project);
        return project;
    }

    @Override
    @Transactional
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        final User user = new User();
        user.setId(userId);
        project.setName(name);
        project.setUser(user);
        projectRepository.save(project);
    }


    @Transactional
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        final User user = new User();
        user.setId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setUser(user);
        projectRepository.save(project);
    }


    @Override
    @Transactional
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.removeByIdAndUserId(userId, project.getId());
    }

    @Override
    @Transactional
    @SneakyThrows
    public Project updateById(@NonNull String userId, @NonNull String id,
                              @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final Project project = projectRepository.findById(userId).orElse(null);
        if (project == null) throw new EntityNotFoundException();
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
        return project;
    }
}
