package ru.tsc.karbainova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.karbainova.tm.model.Session;

import java.util.List;

public interface SessionRepository extends JpaRepository<Session, String> {

    @NotNull List<Session> findAllByUserId(@NotNull String userId);

    void removeById(@Nullable String id);

    void removeByUserId(@NotNull String userId);

}
