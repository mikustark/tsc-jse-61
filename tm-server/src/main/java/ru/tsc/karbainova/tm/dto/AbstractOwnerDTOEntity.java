package ru.tsc.karbainova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.entity.IWBS;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractOwnerDTOEntity extends AbstractDTOEntity implements IWBS {

    @Getter
    @Setter
    @Nullable
    @Column(name = "user_id")
    private String userId;

}
