package ru.tsc.karbainova.tm.service.dto;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.repository.dto.TaskDtoRepository;
import ru.tsc.karbainova.tm.api.service.dto.ITaskService;
import ru.tsc.karbainova.tm.dto.TaskDTO;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class TaskService extends AbstractService<TaskDTO> implements ITaskService {

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        return taskDtoRepository.findAll();
    }

    @Override
    @SneakyThrows
    public List<TaskDTO> findAllTaskByUserId(String userId) {
        return taskDtoRepository.findAllByUserId(userId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void addAll(Collection<TaskDTO> collection) {
        if (collection == null) return;
        for (TaskDTO i : collection) {
            add(i);
        }
    }

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @Transactional
    @SneakyThrows
    public void clear() {
        taskDtoRepository.deleteAll();
    }

    @Override
    @Transactional
    @SneakyThrows
    public TaskDTO add(TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        taskDtoRepository.save(task);
        return task;
    }

    @Override
    @Transactional
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (name == null || name.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskDtoRepository.save(task);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        taskDtoRepository.deleteByUserIdAndId(userId, task.getId());
    }

    @Override
    @Transactional
    @SneakyThrows
    public TaskDTO updateById(@NonNull String userId, @NonNull String id,
                              @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        TaskDTO task = taskDtoRepository.findFirstByUserIdAndId(userId, id);
        if (task == null) throw new ProjectNotFoundException();
        task.setName(name);
        task.setDescription(description);
        taskDtoRepository.save(task);
        return task;
    }

    @Override
    @SneakyThrows
    public TaskDTO findByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskDtoRepository.findFirstByUserIdAndName(userId, name);
    }

    @Override
    @SneakyThrows
    public TaskDTO findByIdUserId(@NonNull String userId, @NonNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        return taskDtoRepository.findFirstByUserIdAndId(userId, id);
    }
}
